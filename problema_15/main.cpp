/*Interseccion de rectangulos, se realiza la busqueda del vertice de la interseccion y luego
  se realiza la resta respectiva e incuentra el ancho y largo de esta interseccion.*/

#include <iostream>

using namespace std;
void imprimirArreglo(float *data);
void calcularInterseccion(float *d1, float *d2,float *dC);

int main()
{
    float data1[]={2,1,8,4}; //Rectangulo A
    float data2[]={6,3,7,7}; //Rectangulo B
    float dataC[4]; //Interseccion del rectangulo A con el B.

    float *pd1=data1; //Puntero al rectangulo A
    float *pd2=data2; //Puntero al rectangulo B
	
    calcularInterseccion(pd1,pd2,dataC); //Funcion que pide el ejercicio, de inteseccion.
	
    cout <<"Data C="; //Formato de salida
	imprimirArreglo(dataC);
	
	return 0;
}

void calcularInterseccion(float *d1, float *d2,float *dC){
    dC[0]=d2[0]; //--->Se encuentra el vertice de la interseccion
    dC[1]=d2[1]; //
    dC[2]=(d1[0]+d1[2])-d2[0]; // Encuentra el ancho de la interseccion
    dC[3]=(d1[1]+d1[3])-d2[1]; // Encuentra el largo de la interseccion
}

void imprimirArreglo(float *data){
    cout <<"{";               //Formato de salida
    for(int i=0;i<4;i++){       //Imprime cada posicion de data separado por una
        cout <<*(data+i)<<",";  // ',' hasta el indice 3 y cierra con }
	}
	cout <<"\b"<< "}";
}
